//
//  CharacteristicData.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation

struct CoreBluetoothCharacteristicData: CBCCharacteristicData {

    let data: Data
    let peripheral: CBCPeripheral
    let identifier: String
}
