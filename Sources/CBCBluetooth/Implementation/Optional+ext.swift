//
//  Optional+ext.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/21/21.
//

import Foundation

extension Optional {
    
    func unwrapped(with error: Error) throws -> Wrapped {
        guard let unwrapped = self else { throw error }
        return unwrapped as Wrapped
    }
}
