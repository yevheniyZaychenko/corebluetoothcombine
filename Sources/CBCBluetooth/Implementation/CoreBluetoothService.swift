//
//  CoreBluetoothService.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation
import CoreBluetooth
import Combine


struct CoreBluetoothService: CBCService {
    
    private let service: CBService
    private let peripheral: CoreBluetoothPeripheral
    
    init(service: CBService, peripheral: CoreBluetoothPeripheral) {
       self.service = service
       self.peripheral = peripheral
    }
    
    func discoverCharacteristics(with uuids: [String]?) -> AnyPublisher<CBCCharacteristic, CBCError> {
        return peripheral.discoverCharacteristics(with: uuids, for: service)
    }
}
