//
//  Peripheral.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation
import Combine

public protocol CBCPeripheral {
    
    var rssi: NSNumber? { get }
    var name: String { get }
    var uuid: UUID { get }
    var connectionState: CurrentValueSubject<Bool, CBCError> { get }
    
    func connect() -> AnyPublisher<CBCPeripheral, CBCError>
    func disconnect() -> AnyPublisher<Void, CBCError>
    
    func discoverServices(with uuids: [String]?) -> AnyPublisher<CBCService, CBCError>
    func observeRSSI() -> AnyPublisher<NSNumber, CBCError>
}
