//
//  CentralManagerBuilder.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/14/21.
//

import Foundation

public final class CBCCentralManagerBuilder {
    
    public static func buildCentralManager() -> CBCCentralManager {
        return CoreBluetoothCentralManager()
    }
}
