//
//  CentralManager.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation
import Combine

public enum CentralManagerPowerState {
    case poweredOn, poweredOff, unspecified
}

public enum CentralManagerAuthorizationState {
    case authorized, unauthorized, unspecified
}

public protocol CBCCentralManager {
    
    var isScanning: Bool { get }
    var powerState: CurrentValueSubject<CentralManagerPowerState, Never> { get }
    var authState: CurrentValueSubject<CentralManagerAuthorizationState, Never> { get }
    
    func startScan(with serviceUUIDs: [String]?) -> AnyPublisher<CBCPeripheral, CBCError>
    func stopScan()
    func disconnectAllPeripherals(for services: [String])
    
    func getPeripherals(with uuids: [UUID]) -> AnyPublisher<CBCPeripheral, CBCError>
    func getConnectedPeripherals(with serviceUUIDs: [String]) -> AnyPublisher<CBCPeripheral, CBCError>
    func observeWillRestoreState(for uuid: UUID) -> AnyPublisher<CBCPeripheral, Never>
}
