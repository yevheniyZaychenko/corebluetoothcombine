//
//  BluetoothError.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//


import Foundation
import CoreBluetooth

public enum CBCError: Error {
    
    case unspecified
    case objectDeallocated
    case invalidData
    case pairing
    
    case connection(Error?)
    case service(Error?)
    case characteristic(Error?)
    case write(Error?)
}

extension Error {
    
    var cbcError: CBCError {
        return self as? CBCError ?? .unspecified
    }
}
