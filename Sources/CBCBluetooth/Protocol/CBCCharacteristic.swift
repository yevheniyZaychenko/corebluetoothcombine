//
//  Characteristic.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation
import Combine

public protocol CBCCharacteristic {
    
    var identifier: String { get }
    
    func readValue() -> AnyPublisher<CBCCharacteristicData, CBCError>
    func observeValue() -> AnyPublisher<CBCCharacteristicData, CBCError>
    func observeNotificationState() -> AnyPublisher<CBCPeripheral, CBCError>
    func writeValue(_ data: Data) -> AnyPublisher<Never, CBCError>
}
