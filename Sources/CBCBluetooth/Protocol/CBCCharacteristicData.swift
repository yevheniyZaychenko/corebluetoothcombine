//
//  CharacteristicData.swift
//  Altos-app
//
//  Created by Eugeniy Zaychenko on 12/13/21.
//

import Foundation

public protocol CBCCharacteristicData {

    var data: Data { get }
    var identifier: String { get }
    var peripheral: CBCPeripheral { get }
}
